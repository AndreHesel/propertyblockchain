/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { PropertyAssetContract } from './property-asset-contract';
export { PropertyAssetContract } from './property-asset-contract';

export const contracts: any[] = [ PropertyAssetContract ];
