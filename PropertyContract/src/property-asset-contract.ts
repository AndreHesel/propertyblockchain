/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract, Info, Returns, Transaction } from 'fabric-contract-api';
import { PropertyAsset } from './property-asset';

@Info({title: 'PropertyAssetContract', description: 'My Smart Contract' })
export class PropertyAssetContract extends Contract {

    @Transaction(false)
    @Returns('boolean')
    public async propertyAssetExists(ctx: Context, propertyAssetId: string): Promise<boolean> {
        const data: Uint8Array = await ctx.stub.getState(propertyAssetId);
        return (!!data && data.length > 0);
    }

    @Transaction()
    public async createPropertyAsset(ctx: Context, propertyAssetId: string, 
                                    type: string, model: string, address : string, postalCode : number, 
                                    buildingArea : number, surfaceArea: number, price : number): Promise<void> {
        const exists: boolean = await this.propertyAssetExists(ctx, propertyAssetId);
        if (exists) {
            throw new Error(`The property asset ${propertyAssetId} already exists`);
        }
        const propertyAsset: PropertyAsset = new PropertyAsset();
        propertyAsset.propertyAssetId = propertyAssetId;
        propertyAsset.model = model;
        propertyAsset.address = address;
        propertyAsset.postalCode = postalCode;
        propertyAsset.price = price;

        const buffer: Buffer = Buffer.from(JSON.stringify(propertyAsset));
        await ctx.stub.putState(propertyAssetId, buffer);

        // AR: 
        const transientMap = ctx.stub.getTransient();
        if (transientMap.get('remark')) {
            await ctx.stub.putPrivateData('productionRemark', propertyAssetId, transientMap.get('remark'));
        }
    }

    @Transaction(false)
    @Returns('PropertyAsset')
    public async readPropertyAsset(ctx: Context, propertyAssetId: string): Promise<PropertyAsset> {
        const exists: boolean = await this.propertyAssetExists(ctx, propertyAssetId);
        if (!exists) {
            throw new Error(`The property asset ${propertyAssetId} does not exist`);
        }
        const data: Uint8Array = await ctx.stub.getState(propertyAssetId);
        const propertyAsset: PropertyAsset = JSON.parse(data.toString()) as PropertyAsset;
       // return propertyAsset;

        try {
            const privBuffer = await ctx.stub.getPrivateData('productionRemark', propertyAssetId);
            PropertyAsset.remark = privBuffer.toString();
            return propertyAsset;
        } catch (error) {
            return propertyAsset;
        }
    }

    @Transaction()
    public async updatePropertyAsset(ctx: Context, propertyAssetId: string, 
                                     buildingArea : number, surfaceArea: number, price : number): Promise<void> {
        const exists: boolean = await this.propertyAssetExists(ctx, propertyAssetId);
        if (!exists) {
            throw new Error(`The property asset ${propertyAssetId} does not exist`);
        }
        const propertyAsset: PropertyAsset = new PropertyAsset();
        propertyAsset.price = price;

        const buffer: Buffer = Buffer.from(JSON.stringify(propertyAsset));
        await ctx.stub.putState(propertyAssetId, buffer);
    }

    @Transaction()
    public async deletePropertyAsset(ctx: Context, propertyAssetId: string): Promise<void> {
        const exists: boolean = await this.propertyAssetExists(ctx, propertyAssetId);
        if (!exists) {
            throw new Error(`The property asset ${propertyAssetId} does not exist`);
        }
        await ctx.stub.deleteState(propertyAssetId);
    }

}
